#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#include "nordic_common.h"
#include "nrf.h"
#include "app_error.h"
#include "ble.h"
#include "ble_hci.h"
#include "ble_srv_common.h"
#include "ble_advdata.h"
#include "ble_advertising.h"
#include "ble_conn_params.h"
#include "nrf_sdh.h"
#include "nrf_sdh_soc.h"
#include "nrf_sdh_ble.h"
#include "app_timer.h"
#include "fds.h"
#include "peer_manager.h"
#include "bsp_btn_ble.h"
#include "sensorsim.h"
#include "ble_conn_state.h"
#include "nrf_ble_gatt.h"
#include "nrf_ble_qwr.h"
#include "nrf_pwr_mgmt.h"
#include "nrf_drv_uart.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

//#include "app_uart.h"
#include "nrf_uart.h"
#include "nrf_delay.h"
#include "bsp.h"
#include "nrf_sdm.h"

//#include "main.h"
#define BLE_UUID_LIGHT 0xBEEF
#define BLE_UUID_SERVICE 0xDEAD
#define BLE_UUID_OUR_BASE_UUID              {0x23, 0xD1, 0x13, 0xEF, 0x5F, 0x78, 0x23, 0x15, 0xDE, 0xEF, 0x12, 0x12, 0x00, 0x00, 0x00, 0x00}



extern ble_gatts_char_handles_t char_handles;

//funzione per aggiungere il servizio con le caratteristiche write
uint16_t Service_Set_ADD(){

    uint32_t err_code;
    ble_gatts_char_handles_t char_handles;

    uint16_t handle;
    
    ble_uuid_t service_uuid;
    ble_uuid128_t base_uuid = BLE_UUID_OUR_BASE_UUID;
    //2 servizio
    service_uuid.type = BLE_GATTS_SRVC_TYPE_PRIMARY;
    service_uuid.uuid = BLE_UUID_SERVICE;

    err_code = sd_ble_uuid_vs_add(&base_uuid, &service_uuid.type);
    APP_ERROR_CHECK(err_code); 

    // OUR_JOB: Add our service
    err_code = sd_ble_gatts_service_add(BLE_GATTS_SRVC_TYPE_PRIMARY,
                                        &service_uuid,
                                        &handle);

    APP_ERROR_CHECK(err_code);  

    return(handle);
}

char Char_Set_ADD(uint16_t handle){

    uint8_t init_value[] = {0, 0, 0, 0};
    ble_gatts_attr_md_t attr_md;
    ble_gatts_attr_t    attr_char_value;
    ble_uuid128_t base_uuid = BLE_UUID_OUR_BASE_UUID;
    ble_gatts_char_handles_t char_handles;

    ret_code_t err_code;

    //impostazione degli attributi globali
    memset(&attr_char_value, 0, sizeof(attr_char_value));
    attr_char_value.init_len = 4;
    attr_char_value.max_len = 4;
    attr_char_value.p_value = &init_value[0];

    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //caratteristica per scrivere la temperatura
    ble_uuid_t          char_light_uuid;  
    ble_gatts_char_md_t char_md;

    char_light_uuid.uuid = BLE_UUID_LIGHT;
    char_light_uuid.type = BLE_UUID_TYPE_UNKNOWN; //BLE_UUID_TYPE_VENDOR_BEGIN;

    err_code = sd_ble_uuid_vs_add(&base_uuid, &char_light_uuid.type);
    APP_ERROR_CHECK(err_code);

    memset(&attr_md, 0, sizeof(attr_md));
    attr_md.vloc = BLE_GATTS_VLOC_STACK;
    
    //attributi specifici
    attr_char_value.p_uuid = &char_light_uuid;
    attr_char_value.p_attr_md   = &attr_md;

    //impostazione dei metadati
    memset(&char_md, 0, sizeof(char_md));
    char_md.char_props.write = 1;
    char_md.char_props.read = 1;
    
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.write_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);

    //configurazione finale
    err_code = sd_ble_gatts_characteristic_add(handle,
                           &char_md,
                           &attr_char_value,
                           &char_handles);

    APP_ERROR_CHECK(err_code);
}


void Light_Service(){

    uint16_t handle_Read, handle_Set;

    handle_Set = Service_Set_ADD();
    Char_Set_ADD(handle_Set);    
}